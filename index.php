<!doctype html>
<html>
  <head>
    <title>Ratchet PHP Socket - Exemplo</title>
    <link rel="stylesheet" href="assets/css/style.css">

  </head>
  <body>
    <li class="chat page">
      <div class="chatArea">
        <ul class="messages"></ul>
      </div>
      <input class="inputMessage" placeholder="Escreva aqui, enter para enviar"/>
    </li>
    <li class="login page">
      <div class="form">
        <h3 class="title">Qual o seu nickname?</h3>
        <input class="usernameInput" type="text" maxlength="14" />
      </div>
    </li>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="assets/js/main.js"></script>
  </body>
</html>
