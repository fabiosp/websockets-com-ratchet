//some javascript was provided get from https://github.com/socketio/socket.io/tree/master/examples/chat
$(function() {

  //cria a conexao
  var conn = new WebSocket('ws://localhost:8080?identifier=999'); //identifier caso deseja enviar mais dados para a conexao no PHP
      conn.onopen = function(e) {
      var $el = $('<li>').addClass('log').text("Sistema: Online!");
      $messages.append($el);
  };

  //verifica novas mensagens enviadas
  conn.onmessage = function(e) {
    var $el = $('<li>').addClass('log').text(e.data);
    $messages.append($el);
  };

  var username;
  var $window = $(window);
  var $usernameInput = $('.usernameInput'); // Input for username
  var $messages = $('.messages'); // Messages area
  var $inputMessage = $('.inputMessage'); // Input message input box
  var $loginPage = $('.login.page'); // The login page
  var $chatPage = $('.chat.page'); // The chatroom page
  var $currentInput = $usernameInput.focus();

  //envia a mensagem para o socket
  function enviaMensagemSocket(msg){
    if(msg != ""){
      msg = username+' disse: '+msg;
      msg = JSON.stringify({cmd: 'SEND', chat: 'sala', message: msg}); //enviando um objeto JSON a fimd e enviar mais dados alem da mensagem
      var $el = $('<li>').addClass('log').text(msg.message);
      $messages.append($el);
      conn.send(msg);     
    }
  }


  function setUsername () {
    username = cleanInput($usernameInput.val().trim());

    if (username) {
      $loginPage.fadeOut();
      $chatPage.show();
      $loginPage.off('click');
      $currentInput = $inputMessage.focus();
      
    }
  }

  // Prevents input from having injected markup
  function cleanInput (input) {
    return $('<div/>').text(input).text();
  }

  $window.keydown(function (event) {
    // Auto-focus the current input when a key is typed
    if (!(event.ctrlKey || event.metaKey || event.altKey)) {
      $currentInput.focus();
    }
    // When the client hits ENTER on their keyboard
    if (event.which === 13) {
      if (username) {
        var msg = cleanInput($inputMessage.val().trim());
        enviaMensagemSocket(msg);
        $inputMessage.val("");        
      } else {
        setUsername();
      }
    }
  });



});
