<?php
namespace MyApp;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class Chat implements MessageComponentInterface {
    protected $clients;

    public function __construct() {
        $this->clients = new \SplObjectStorage;
    }

    public function onOpen(ConnectionInterface $conn) {
        //de desejar pode repassar na coenxao parametros para verificacoes extras antes de
        //adicionar a conexao na lista com attach()
        //exemplo no javascript: var conn = new WebSocket('ws://localhost:8080?identifier=999');
        $dataConexao = $conn->WebSocket->request->getQuery()->toArray(); //retorna os parametros da URL
        //echo $dataConexao['identifier']; //apresenta o 999

        // Store the new connection to send messages to later
        if( isset($dataConexao['identifier']) ){
            if($dataConexao['identifier'] == 999){
                $this->clients->attach($conn);
                echo "Nova conexao! ({$conn->resourceId})\n";
            }
        }        
       
    }

    public function onMessage(ConnectionInterface $from, $msg) {        
        $numRecv = count($this->clients) - 1;
        //if($numRecv < 0) return false; //caso nao possuir outros clientes

        $msg = json_decode($msg); //aqui temos o bojeto json para autenticacao/verificaoes

        echo sprintf('A conexao %d enviou a mensagem "%s" para %d outra%s conex%s' . "\n"
            , $from->resourceId, $msg->message, $numRecv, $numRecv == 1 ? '' : 's' ,$numRecv == 1 ? 'ao' : 'oes');

        foreach ($this->clients as $client) {
            //if ($from !== $client) { // The sender is not the receiver, send to each client connected            
            $client->send($msg->message);
            //}
        }
    }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);        
        echo "{$conn->resourceId} Desconectou\n";

        // if( count($this->clients) == 0){
        //     echo 'todos desconectaram';
        //     unset($this);
        // }
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "Um erro ocorreu: {$e->getMessage()}\n";

        $conn->close();
    }
}
