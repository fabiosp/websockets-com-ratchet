# Websockets - Chat - Ratchet
Aplicação de referência para o uso de websockets com a lib [Ratchet](http://socketo.me). Para real-time updates veja última seção.


## Instalação e Conexão

```bash
$ composer install
$ php chat-server.php
```
E está pronto. Aponte vários navegadores para sua URL de instalação do projeto e ~~converse com os seus vários eus interiores~~ comece a conversar! Verifique o arquivo /assets/js/main.js e app/Chat.php para descobrir a codificação nas interações com websocket.

![Exemplo](assets/img/exemplo.png "Ratchet em execução")




##Adicionando em Frameworks

### Laravel


* Instale normalmente o pacote Ratchet via composer (composer require composer require cboden/ratchet).
* Pode reutilizar a classe app\Chat.php (deste projeto) no Laravel basta incluí-la como classe Externa. Para isso basta inserir a classe dentro de app, pode criar ser dentro de Providers ou outros. Crie, por exemplo, um diretório novo chamado ChatApp (resultando em app/ChatApp/Chat.php).
* Concluído o passo anterior, será capaz de incluir as classes que instanciam o socket. Agora podemos criar um comando pelo Artisan do Laravel:


```bash
$ php artisan make:console ChatServer --command=chat:open
```

* Onde um arquivo ChatServer.php será criado em app/Console/Commands, nela, codifique o $server->run(), conforme foi realizado em chat-server.php deste projeto. Após isso, inclua em app/Console/Kernel.php a localização do seu novo comando. Pronto, agora você tem um novo comando no Artisan conforme imagem abaixo:

![Laravel](assets/img/inlaravel.png "Comando no Laravel")

* Pode criar o comando de outras formas. Execute php artisan chat:open para abrir o socket.
* Após isso, crie uma rota que retorne uma view com as mesmas informações que o index.php deste projeto, onde consta o Javascript que procura e utiliza o websocket.
* Mais documentações, procure no próprio site da [Ratchet](http://socketo.me). 




## Real-time updates com Pusher

No chat anterior aprendemos a criar uma aplicação com websocket. Imagine agora que o seu problema seja acompanhar em tempo real a postagem de comentários em um post de qualquer blog. Não existe uma ligação direta entre usuários, mas com a aplicação. Então você precisa do Pusher! Toda a aplicação consta em /blog:

* Adicione no composer em require o pacote: "react/zmq": "0.2.*|0.3.*"
* Este pacote depende do [ZeroMQ](http://www.zeromq.org/) para que um script PHP possa pegar atualizações de outro script PHP. O pacote em questão demanda a extensão extension=zmq.so ativa no seu php.ini para CLI. Para resolver os pacotes necessários no sistema basta executar

```bash
$ sudo apt-get install libtool pkg-config build-essential autoconf automake zmq-beta php-pear php5-dev
$ sudo pecl install zmq-beta
$ sudo /etc/init.d/apache2 restart
```

* Se você desenvolve em Windows, ~~aconselho a criar vergonha na cara e formatar isso aí~~ aconselho a verificar modos de instalação no site do ZeroMQ. Após reiniciar o servidor Apache, estará tudo pronto.
* Executar php push-server.php subirá o serviço.
* Abra o navegador e aponte para /blog/index.php
* Abra outro navegador ou faça uma requisição $_POST para /blog/post.php
* Veja no console no seu primeiro navegador a recepção dos dados codificados em JSON. Use como referência o chat para proveitar os dados e mostrar na tela as atualizações, com um visual mais interessante.

Divirta-se!