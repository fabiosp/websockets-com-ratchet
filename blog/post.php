<?php
$entryData = array(
	'category' => 'kittensCategory',
	'title'    => 'Titulo teste',
	'article'  => 'texto do post',
	'when'     => time()
);
$context = new ZMQContext();
$socket = $context->getSocket(ZMQ::SOCKET_PUSH, 'my pusher');
$socket->connect("tcp://localhost:5555");

$socket->send(json_encode($entryData));